/** @file
	Main module header
*/

#ifndef __MAIN_H
#define __MAIN_H

#define LED_Pin GPIO_PIN_13
#define LED_Port GPIOC

#define FSP_Pin GPIO_PIN_12
#define FSP_Port GPIOA

#define AD0_Pin GPIO_PIN_0
#define AD0_Port GPIOB

#define SW0_Pin GPIO_PIN_1
#define SW0_Port GPIOB

#define ECA_Pin GPIO_PIN_6
#define ECA_Port GPIOB
#define ECB_Pin GPIO_PIN_7
#define ECB_Port GPIOB

#endif /* __MAIN_H */
