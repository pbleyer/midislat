/** @file
*/

#ifndef __USBD_CDC_IF_H
#define __USBD_CDC_IF_H

#ifdef __cplusplus
 extern "C" {
#endif

#include "usbd_midi.h"
	 
extern USBD_MIDI_ItfTypeDef USBD_Interface_fops_FS;

uint8_t MIDI_Transmit_FS(uint8_t* Buf, uint16_t Len);

#ifdef __cplusplus
} // extern "C"
#endif
  
#endif /* __USBD_CDC_IF_H */
