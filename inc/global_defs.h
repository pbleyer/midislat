/** @file
	Global definitions
	@author pbleyer
*/

#ifndef GLOBAL_DEFS_H_
#define GLOBAL_DEFS_H_

/** @defgroup globaldef Global definitions */
///@{

/** Return value on success (zero) */
#define OK 0

/** Return value on error (negative)  */
#define ERRCODE(x) (-(x))

/** Return value on warning (positive)  */
#define WARNCODE(x) (x)

/** Join two bytes into high/low value */
#define byteJoin(a, b) (((a)<<8)|(b))

/** Get low byte of value (16-bit) */
#define byteLow(a) ((a) & 0xff)

/** Get high byte of value (16-bit) */
#define byteHigh(a) (((a)>>8) & 0xff)

/** Get n-th byte of value */
#define byteGet(a,n) (((a)>>(8*(n))) & 0xff)

/** Minimum macro */
#define minimum(a,b) (((a)<(b)) ? (a) : (b))

/** Maximum macro */
#define maximum(a,b) (((a)>(b)) ? (a) : (b))

/** Coerce to bounds macro */
#define coerce(x,mn,mx) (((x)<(mn)) ? (mn) : (((x)>(mx)) ? (mx) : (x)))

/** Round x to next multiple of power of 2 */
#define rmulp2(x,y) (((x) & ~((1<<(y))-1)) + (((x) & ((1<<(y))-1)) ? (1<<(y)) : 0))

/** Clear object */
#define clearObject(p) memset((p), 0, sizeof(*(p)))

/** Set (v == !0) or clear (v == 0) bit b of value p */
#define bitSet(p,b,v) ((v) ? ((p) |= (1<<(b))) : ((p) &= ~(1<<(b))))

/** Compare bit; !0 if bit b of value p is set, 0 otherwise */
#define bitIs(p,b) ((p) & (1<<(b)))

/** Get bit b from value p (0 or 1) */
#define bitGet(p,b) (bitIs((p),(b)) ? 1 : 0)

/** Set (v == !0) or clear (v == 0) bits b from value p */
#define maskSet(p,m,v) ( (v) ? ((p) |= (m)) : ((p) &= ~(m)) )

/** Compare bits, !0 if bits b of value p are set, 0 otherwise */
#define maskIs(p,m) ((p) & (m))

/** Convert bit position to bitmask */
#define bM(b) (1<<(b))

/** Concatenate names */
#define __defcat(a,b) a##b

/** Convert bit suffix to bit_X definition */
#define __bitdef(s) bit_##s

/** Convert port suffix to prt_X definition */
#define __prtdef(s) prt_##s

/** Convert bit number to GPIO_Pin_X definition */
#define __itmpin(n) __defcat(GPIO_PIN_,n)

/** Convert port letter to GPIOX definition */
#define __itmprt(n) __defcat(GPIO,n)

/** Convert bit suffix to GPIO_Pin_X definition */
#define pin(b) __itmpin(__bitdef(b))

/** Convert port suffix to GPIOX definition */
#define prt(p) __itmprt(__prtdef(p))

/** Expand to port/pin comma-separated list
	@param a Suffix of the port/pin definition
*/
#define prtpin(a) prt(a), pin(a)

/** Convert bit number to EXTI line definition */
#define __itmext(n) __defcat(EXTI_PR_PR,n)

/** Expand bit to external interrupt line (EXTI) */
#define ext(b) __itmext(__bitdef(b))

/** RTOS ticks from microseconds */
#define usTicks(us) ((us)/((TickType_t)1000000/configTICK_RATE_HZ))

/** RTOS ticks from milliseconds */
#define msTicks(ms) pdMS_TO_TICKS(ms)

///@}

#endif // GLOBAL_DEFS_H_
