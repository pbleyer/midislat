/** @file
*/

#ifndef __MAIN_H
#define __MAIN_H

#define LED_Pin GPIO_PIN_13
#define LED_GPIO_Port GPIOC

#define FS_DP_Pin GPIO_PIN_12
#define FS_DP_Port GPIOA

#define ADC1_IN0_Pin GPIO_PIN_0
#define ADC1_IN0_Port GPIOA

#endif // __MAIN_H
