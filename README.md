# README #

A quick hack to get an analog expression/volume pedal connected to a computer
through USB MIDI using vcc-gnd cheap
[STM32F103C8T6 prototype board](http://www.vcc-gnd.com/cp/stm32hxb/stm32f1xxhexinban/).

## Setup ##

* Compile Keil uVision project.
* Connect pedal potentiometer output to PB0 (ADC1:IN0).
* Connect to computer using uUSB cable.
