/**
  ******************************************************************************
  * @file    usbd_cdc.h
  * @author  MCD Application Team
  * @version V2.4.2
  * @date    11-December-2015
  * @brief   header file for the usbd_cdc.c file.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USB_MIDI_H
#define __USB_MIDI_H

#ifdef __cplusplus
 extern "C" {
#endif

#include  "usbd_ioreq.h"

#define MIDI_IN_EP 0x81  /* EP1 for data IN */
#define MIDI_OUT_EP 0x01  /* EP1 for data OUT */
// #define MIDI_CMD_EP 0x82  /* EP2 for MIDI commands */

/* MIDI Endpoints parameters: you can fine tune these values depending on the needed baudrates and performance. */
#define MIDI_DATA_HS_MAX_PACKET_SIZE 512  /* Endpoint IN & OUT Packet size (HS) */
#define MIDI_DATA_FS_MAX_PACKET_SIZE 64  /* Endpoint IN & OUT Packet size (FS) */
// #define MIDI_CMD_PACKET_SIZE 8  /* Control Endpoint Packet size */

#define USB_MIDI_CONFIG_DESC_SIZ 101
#define MIDI_DATA_HS_IN_PACKET_SIZE MIDI_DATA_HS_MAX_PACKET_SIZE
#define MIDI_DATA_HS_OUT_PACKET_SIZE MIDI_DATA_HS_MAX_PACKET_SIZE

#define MIDI_DATA_FS_IN_PACKET_SIZE MIDI_DATA_FS_MAX_PACKET_SIZE
#define MIDI_DATA_FS_OUT_PACKET_SIZE MIDI_DATA_FS_MAX_PACKET_SIZE

/* MIDI definitions */
#define MIDI_CN(x) (((x)>>4) & 0xf) ///< Get MIDI cable number from CN/CIN
#define MIDI_CIN(x) ((x) & 0xf) ///< Get MIDI code index number
/* CIn codes */
#define MIDI_MISC 0x0 ///< Miscellaneous, reserved (1B, 2B, 3B)
#define MIDI_CEV 0x1 ///< Cable event, reserved (1B, 2B, 3B)
#define MIDI_SC2 0x2 ///< 2-byte system common message (MTC, song select, etc) (2B)
#define MIDI_SC3 0x3 ///< 3-byte system common message (SPP, etc) (3B)
#define MIDI_SX 0x4 ///< SysEx starts/continues (3B)
#define MIDI_SC1 0x5 ///< Single-byte system common message or SysEx ends with following single byte (1B)
#define MIDI_SX2 0x6 ///< SysEx ends with following 2 bytes (2B)
#define MIDI_SX3 0x7 ///< SysEx ends with following 3 bytes (3B)
#define MIDI_OFF 0x8 ///< MIDI note off (3B)
#define MIDI_ON 0x9 ///< MIDI note on (3B)
#define MIDI_PLY 0xa ///< MIDI poly key press (3B)
#define MIDI_CC 0xb ///< MIDI control change (3B)
#define MIDI_PC 0xc ///< MIDI program change (2B)
#define MIDI_CP 0xd ///< MIDI channel pressure (2B)
#define MIDI_PB 0xe ///< MIDI pitch bend change (3B)
#define MIDI_SB 0xf ///< MIDI single byte (bypasses parsing) (1B)


/* MIDI streaming */
#define MIDI_UNDEFINED 0x00 ///< Descriptor, jack, EP control
#define MIDI_MS_HEADER 0x01
#define MIDI_IN_JACK 0x02
#define MIDI_OUT_JACK 0x03
#define MIDI_ELEMENT 0x04
#define MIDI_MS_GENERAL 0x01
#define MIDI_EMBEDDED 0x01 ///< Embedded jack
#define MIDI_EXTERNAL 0x02 ///< External jack

#define MIDI_ASSOCIATION_CONTROL 0x01 ///< EP association control

#pragma anon_unions

typedef union
{
	uint32_t event;
	struct
	{	
		uint8_t cc; ///< [7:4] Cable Number, [3:0] Code Index
		uint8_t b0; ///< Byte 0
		uint8_t b1; ///< Byte 1
		uint8_t b2; ///< Byte 2
	};
} USBD_MIDI_Event;

typedef struct _USBD_MIDI_Itf
{
  int8_t (*Init)(void);
  int8_t (*DeInit)(void);
  int8_t (*Control)(uint8_t, uint8_t *, uint16_t);
  int8_t (*Receive)(uint8_t *, uint32_t *);
} USBD_MIDI_ItfTypeDef;

typedef struct
{
  uint32_t data[MIDI_DATA_FS_MAX_PACKET_SIZE/4]; /* Force 32bits alignment */ // MIDI_DATA_HS_MAX_PACKET_SIZE
  uint8_t  CmdOpCode;
  uint8_t  CmdLength;
  uint8_t  *RxBuffer;
  uint8_t  *TxBuffer;
  uint32_t RxLength;
  uint32_t TxLength;

  __IO uint32_t TxState;
  __IO uint32_t RxState;
}
USBD_MIDI_HandleTypeDef;

extern USBD_ClassTypeDef USBD_MIDI;
#define USBD_MIDI_CLASS &USBD_MIDI

uint8_t  USBD_MIDI_RegisterInterface(USBD_HandleTypeDef *pdev, USBD_MIDI_ItfTypeDef *fops);
uint8_t  USBD_MIDI_SetTxBuffer(USBD_HandleTypeDef *pdev, uint8_t *pbuff, uint16_t length);
uint8_t  USBD_MIDI_SetRxBuffer(USBD_HandleTypeDef *pdev, uint8_t *pbuff);
uint8_t  USBD_MIDI_ReceivePacket(USBD_HandleTypeDef *pdev);
uint8_t  USBD_MIDI_TransmitPacket(USBD_HandleTypeDef *pdev);

#ifdef __cplusplus
} // extern "C"
#endif

#endif  /* __USB_MIDI_H */
